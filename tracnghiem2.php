<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="tracnghiem.css">
    <title>Document</title>
</head>
<body>
    <?php
    session_start();
    // $ans_page_1 = $_SESSION

    // $question = array(1 => "Đâu là hang động tự nhiên lớn nhất thế giới?", 
    // 2 => "Cực Đông của Việt Nam nằm ở tỉnh nào",
    // 3 => "Tỉnh duy nhất có ba mặt đất liền giáp biển?", 
    // 4 => "Địa phương nào trồng nhiều chè nhất cả nước?", 
    // 5 => "Tỉnh nào có mật độ dân số thấp nhất cả nước?", 
    // 6 => "Tỉnh nào có mật độ dân số cao nhất Việt Nam?", 
    // 7 => "Dưới thời nhà Nguyễn, quần đảo Hoàng Sa thuộc tỉnh nào?", 
    // 8 => "Sách Toàn tập Thiên Nam tứ chí lộ đồ thư gồm mấy quyên?", 
    // 9 => "Việt Nam có đường bờ biển trải dài bao nhiêu km?", 
    // 10 => "Vĩ tuyển nào chia đôi nước ta trong giai đoạn năm 1954?");

    // $answer = array(
    //     1 => array("Sơn Đoòng" => 1,
    //             "Thiên Cung" => 0 ,
    //             "Phong Nha" => 0,
    //             "Trầm" => 0),
    //     2 => array("Khánh Hòa" => 1,
    //        "Ninh Thuận" => 0,
    //         "Phú Yên" => 0,
    //         "Phan Thiết" => 0),
    //     3 => array("Khánh Hòa" => 0,
    //         "Kiên Giang" => 1,
    //         "Cân Thơ" => 0,
    //         "Cà Mau" => 0),
    //     4 => array("Quảng Nam" => 0,
    //         "Thái Nguyên" => 1,
    //         "Vĩnh Phúc" => 0,
    //         "Lâm Đồng" => 0),
    //     5 => array("Hà Giang" => 0,
    //         "Lai Châu" => 0,
    //         "Điện Biên" => 1,
    //         "Bắc Kan" => 0),
    //     6 => array("Hà Nam" => 0,
    //         "Nam Định" => 0,
    //         "Hà Nội" => 0,
    //         "Bắc Ninh" => 1),
    //     7 => array("Quảng Bình" => 0,
    //         "Quảng Ngãi"  => 1,
    //         "Quảng Ngãi" => 0,
    //         "Khánh Hòa" => 0),
    //     8 => array("4 quyển" => 1,
    //         "5 quyển" => 0,
    //         "6 quyển" => 0,
    //         "7 quyển" => 0),
    //     9 => array("Hơn 2.260 km" => 0,
    //         "Hon 3.260 km" => 1,
    //         "Hơn 4.260 km" => 0,
    //         "Hơn 5.260 km" => 0),
    //     10 => array("16" => 0,
    //         "18" => 0,
    //         "19" => 0,
    //         "17" => 1));

    $question = $_SESSION["question"];
    $answer = $_SESSION["answer"];
    $page_1 = $_SESSION;
    
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $_SESSION = $page_1;
        for ($i=6; $i <= 10; $i++) { 
            $question_key = "question_".strval($i);
            $_SESSION[$question_key]=$_POST[$question_key];
        }
        header("Location: submit.php");
    }
    ?>

<form method="POST" enctype="multipart/form-data" action="<?php 
         echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <?php
        foreach($question as $question_key => $question_value) {
            if ($question_key >= 6) {
                echo "<p><b>{$question_key}. {$question_value}</b><p>";
                foreach ($answer as $answer_key => $answer_value) {
                    if ($question_key == $answer_key) {
                        foreach ($answer_value as $answer_value_value => $answer_value_key) {
                            echo "<input type=\"radio\" name=\"question_{$answer_key}\" value = \"{$answer_value_value}\">{$answer_value_value}<br>";
                        }
                    }
                }
            }
        }
    ?>
    <button>NỘP BÀI</button>
        
</form>


</body>
</html>